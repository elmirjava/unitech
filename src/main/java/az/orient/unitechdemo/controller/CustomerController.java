package az.orient.unitechdemo.controller;

import az.orient.unitechdemo.dto.response.RespCustomer;
import az.orient.unitechdemo.dto.response.Response;
import az.orient.unitechdemo.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;
    @GetMapping
    public Response<List<RespCustomer>> getCustomerList() {
        return customerService.getCustomerList();
    }
}
