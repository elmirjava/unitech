package az.orient.unitechdemo.controller;

import az.orient.unitechdemo.dto.request.ReqTransaction;
import az.orient.unitechdemo.dto.response.RespTransaction;
import az.orient.unitechdemo.dto.response.Response;
import az.orient.unitechdemo.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;
    @PostMapping("GetTransactionListByAccountId/{id}")
    public Response<List<RespTransaction>> getTransactionList(@RequestBody ReqTransaction reqTransaction){

        return transactionService.getTransactionListByAccountId(reqTransaction);
    }
    @PostMapping("AddTransaction")
    public Response addTransaction(@RequestBody ReqTransaction reqTransaction){

        return transactionService.makeTransaction(reqTransaction);
    }

}
