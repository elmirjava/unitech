package az.orient.unitechdemo.controller;

import az.orient.unitechdemo.dto.request.ReqUser;
import az.orient.unitechdemo.dto.response.Response;
import az.orient.unitechdemo.service.RegisterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/register")
public class RegisterController {
    private final RegisterService registerService;
    @PostMapping
    private Response register(@RequestBody ReqUser reqUser){
        return registerService.register(reqUser);
    }
}
