package az.orient.unitechdemo.controller;

import az.orient.unitechdemo.dto.response.RespAccount;
import az.orient.unitechdemo.dto.response.Response;
import az.orient.unitechdemo.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {
    private final AccountService accountService;
    @PostMapping("/GetAccountListByCustomerId/{id}")
    public Response<List<RespAccount>> getAccountListByCustomerId(@PathVariable Long customerId){
        return accountService.getAccountListByCustomerId(customerId);
    }
}
