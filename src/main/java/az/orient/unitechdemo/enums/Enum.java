package az.orient.unitechdemo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
public enum Enum {
    Active(1),Deactive(0);
    private int value;

}
