package az.orient.unitechdemo.dto.response;

import lombok.Data;


@Data

public class RespUser {
    private String pin;
    private String password;
}
