package az.orient.unitechdemo.dto.request;

import az.orient.unitechdemo.dto.response.RespAccount;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReqTransaction {
    private Long id;
    private String receiptNo;
    private RespAccount dtAccount;
    private String crAccount;
    private Double amount;
}
