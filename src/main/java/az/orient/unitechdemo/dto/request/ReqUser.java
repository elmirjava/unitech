package az.orient.unitechdemo.dto.request;

import lombok.Data;

@Data
public class ReqUser {
    private String pin;
    private String password;
}
