package az.orient.unitechdemo.repository;
import az.orient.unitechdemo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findByPinAndActive(String pin,Integer active);
    User findByPinAAndPasswordAndActive(String pin,String password,Integer active);



}
