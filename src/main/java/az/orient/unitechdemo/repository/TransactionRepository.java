package az.orient.unitechdemo.repository;

import az.orient.unitechdemo.entity.Account;
import az.orient.unitechdemo.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction,Long> {
    List<Transaction>findAllByDtAccountAndActive(Account account, Integer active);

}
