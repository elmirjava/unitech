package az.orient.unitechdemo.repository;

import az.orient.unitechdemo.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping
public interface CustomerRepository extends JpaRepository<Customer,Long> {
    List<Customer> findAllByActive(Integer active);
    Customer findCustomerByIdAndActive(Long id,Integer active);
}
