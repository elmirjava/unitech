package az.orient.unitechdemo.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UniTechException extends RuntimeException{
     private Integer code;
    public UniTechException(Integer code,String message){
        super(message);
        this.code=code;
    }
    public Integer getCode(){
        return code;
    }
}
