package az.orient.unitechdemo.exception;

import az.orient.unitechdemo.dto.response.RespStatus;

public final class ExceptionConstants {
    public static final Integer INVALID_REQUEST_DATA=100;
    public static final Integer USER_NOT_FOUND = 101;
    public static final Integer USER_EXIST = 102;
    public static final Integer INTERNAL_EXCEPTION = 103;
    public static final Integer CUSTOMER_NOT_FOUND=104;
    public static final Integer ACCOUNT_NOT_FOUND=105;
    public static final Integer TRANSACTION_NOT_FOUND=106;
    public static final Integer THERE_IS_NOT_ENOUGH_AMOUNT = 107;
    public static final Integer DEACTIVE_ACCOUNT = 108;
}
