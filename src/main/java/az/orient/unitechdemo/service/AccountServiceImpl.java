package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.response.RespAccount;
import az.orient.unitechdemo.dto.response.RespCustomer;
import az.orient.unitechdemo.dto.response.RespStatus;
import az.orient.unitechdemo.dto.response.Response;
import az.orient.unitechdemo.entity.Account;
import az.orient.unitechdemo.entity.Customer;
import az.orient.unitechdemo.enums.Enum;
import az.orient.unitechdemo.exception.ExceptionConstants;
import az.orient.unitechdemo.exception.UniTechException;
import az.orient.unitechdemo.repository.AccountRepository;
import az.orient.unitechdemo.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService{
    private final CustomerRepository customerRepository;
    private final AccountRepository accountRepository;
    @Override
    public Response<List<RespAccount>> getAccountListByCustomerId(Long customerId) {
        Response<List<RespAccount>> response = new Response<>();
        try{
            if(Objects.isNull(customerId)){
                throw new UniTechException(ExceptionConstants.CUSTOMER_NOT_FOUND,"Customer not found");
            }
            Customer customer = customerRepository.findCustomerByIdAndActive(customerId, Enum.Active.getValue());
            if (customer == null) {
                throw new UniTechException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not found");
            }
            List<Account> accountList = accountRepository.findAllByCustomerAndActive(customer, Enum.Active.getValue());
            if (accountList.isEmpty()) {
                throw new UniTechException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            List<RespAccount> respAccountList=accountList.stream().map(this::convert).collect(Collectors.toList());
            response.setT(respAccountList);
            response.setStatus(RespStatus.succesMessage());
        }catch (UniTechException uniTechException) {
            uniTechException.printStackTrace();
            response.setStatus(new RespStatus(uniTechException.getCode(), uniTechException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }
    public RespAccount convert(Account account) {
        RespCustomer respCustomer=RespCustomer.builder()
                .id(account.getCustomer().getId())
                .name(account.getCustomer().getName())
                .surname(account.getCustomer().getSurname())
                .build();
        RespAccount respAccount = RespAccount.builder()
                .id(account.getId())
                .name(account.getName())
                .accountNo(account.getAccountNo())
                .iban(account.getIban())
                .amount((double) account.getAmount()/100)
                .currency(account.getCurrency())
                .respCustomer(respCustomer)
                .build();
        return respAccount;
    }
}
