package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.response.RespCustomer;
import az.orient.unitechdemo.dto.response.RespStatus;
import az.orient.unitechdemo.dto.response.Response;
import az.orient.unitechdemo.entity.Customer;
import az.orient.unitechdemo.enums.Enum;
import az.orient.unitechdemo.exception.ExceptionConstants;
import az.orient.unitechdemo.exception.UniTechException;
import az.orient.unitechdemo.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService{
    private final CustomerRepository customerRepository;
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public Response<List<RespCustomer>> getCustomerList() {
        Response<List<RespCustomer>> response = new Response<>();
        try{
            List<Customer> customerList = customerRepository.findAllByActive(Enum.Active.getValue());
            if (customerList.isEmpty()) {
                throw new UniTechException(ExceptionConstants.CUSTOMER_NOT_FOUND, "Customer not found!!!");
            }
            List<RespCustomer> responseCustomers = customerList.stream().map(customer ->
                    convert(customer)).collect(Collectors.toList());
            response.setT(responseCustomers);

            response.setStatus(RespStatus.succesMessage());
        }catch (UniTechException uniTechException) {
            uniTechException.printStackTrace();
            response.setStatus(new RespStatus(uniTechException.getCode(), uniTechException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }
    private RespCustomer convert(Customer customer) {
        RespCustomer respCustomer = RespCustomer.builder().
                id(customer.getId()).
                name(customer.getName()).
                surname(customer.getSurname()).
                address(customer.getAddress()).
                dob(customer.getDob() != null ? df.format(customer.getDob()) : null).
                phone(customer.getPhone()).
                cif(customer.getCif()).
                seria(customer.getSeria()).
                build();
        return respCustomer;
    }
}
