package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.request.ReqUser;
import az.orient.unitechdemo.dto.response.Response;

public interface RegisterService {
    Response register(ReqUser reqUser);
}
