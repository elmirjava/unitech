package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.request.ReqUser;
import az.orient.unitechdemo.dto.response.RespStatus;
import az.orient.unitechdemo.dto.response.Response;
import az.orient.unitechdemo.entity.User;
import az.orient.unitechdemo.enums.Enum;
import az.orient.unitechdemo.exception.ExceptionConstants;
import az.orient.unitechdemo.exception.UniTechException;
import az.orient.unitechdemo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RegisterServiceImpl implements RegisterService {
    private final UserRepository userRepository;

    @Override
    public Response register(ReqUser reqUser) {
        Response response=new Response();
        try {
            String pin = reqUser.getPin();
            String password = reqUser.getPassword();
            if (Objects.isNull(pin) || Objects.isNull(password)) {
                throw new UniTechException(ExceptionConstants.INVALID_REQUEST_DATA, "Invalid Request Data");
            }
            User user=userRepository.findByPinAndActive(pin, Enum.Active.getValue());
            if (user!=null) {
                throw new UniTechException(ExceptionConstants.USER_EXIST, "User With Pin Is Exist");
            }
            User user2=new User();
            user2.setPin(pin);
            user2.setPassword(password);
            userRepository.save(user2);
            response.setStatus(RespStatus.succesMessage());
        } catch (UniTechException uniTechException) {
            uniTechException.printStackTrace();
            response.setStatus(new RespStatus(uniTechException.getCode(), uniTechException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }
}
