package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.request.ReqTransaction;
import az.orient.unitechdemo.dto.response.RespTransaction;
import az.orient.unitechdemo.dto.response.Response;

import java.util.List;

public interface TransactionService {
    Response makeTransaction(ReqTransaction reqTransaction);

    Response<List<RespTransaction>> getTransactionListByAccountId(ReqTransaction reqTransaction);
}
