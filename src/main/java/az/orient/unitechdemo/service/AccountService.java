package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.response.RespAccount;
import az.orient.unitechdemo.dto.response.Response;

import java.util.List;

public interface AccountService {
    Response<List<RespAccount>> getAccountListByCustomerId(Long customerId);
}
