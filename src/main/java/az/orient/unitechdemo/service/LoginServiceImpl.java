package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.request.ReqUser;
import az.orient.unitechdemo.dto.response.RespStatus;
import az.orient.unitechdemo.dto.response.Response;
import az.orient.unitechdemo.entity.User;
import az.orient.unitechdemo.enums.Enum;
import az.orient.unitechdemo.exception.ExceptionConstants;
import az.orient.unitechdemo.exception.UniTechException;
import az.orient.unitechdemo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService{
    private final UserRepository userRepository;
    @Override
    public Response login(ReqUser reqUser) {
        Response response=new Response();
        try {
            String pin=reqUser.getPin();
            String password= reqUser.getPassword();
            if(Objects.isNull(pin)||Objects.isNull(password)){
                throw new UniTechException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid Request Data");
            }
            User user=userRepository.findByPinAAndPasswordAndActive(pin,password, Enum.Active.getValue());
            if(Objects.isNull(user)){
                throw new UniTechException(ExceptionConstants.USER_NOT_FOUND,"User Not Found");
            }
            response.setStatus(RespStatus.succesMessage());
        }catch (UniTechException uniTechException) {
            uniTechException.printStackTrace();
            response.setStatus(new RespStatus(uniTechException.getCode(), uniTechException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }
}
