package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.response.RespCustomer;
import az.orient.unitechdemo.dto.response.Response;

import java.util.List;

public interface CustomerService {
    Response<List<RespCustomer>>getCustomerList();
}
