package az.orient.unitechdemo.service;

import az.orient.unitechdemo.dto.request.ReqTransaction;
import az.orient.unitechdemo.dto.response.*;
import az.orient.unitechdemo.entity.Account;
import az.orient.unitechdemo.entity.Transaction;
import az.orient.unitechdemo.enums.Enum;
import az.orient.unitechdemo.exception.ExceptionConstants;
import az.orient.unitechdemo.exception.UniTechException;
import az.orient.unitechdemo.repository.AccountRepository;
import az.orient.unitechdemo.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService{
    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;

    @Override
    public Response makeTransaction(ReqTransaction reqTransaction) {
        Response response=new Response();
        try{
            Long accountId = reqTransaction.getDtAccount().getId();
            String receiptNo = reqTransaction.getReceiptNo();
            if (accountId == null || receiptNo == null) {
                throw new UniTechException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            Account account = accountRepository.findAccountByIdAndActive(reqTransaction.getDtAccount().getId(), Enum.Active.getValue());
            if (account == null) {
                throw new UniTechException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            Transaction transaction=new Transaction();
            transaction.setReceiptNo(reqTransaction.getReceiptNo());
            if(account.getActive()==Enum.Deactive.getValue()){
                throw new UniTechException(ExceptionConstants.DEACTIVE_ACCOUNT,"DEactive Account");
            }
            transaction.setDtAccount(account);
            transaction.setCrAccount(reqTransaction.getCrAccount());
            if(account.getAmount()< reqTransaction.getAmount()){
                throw new UniTechException(ExceptionConstants.THERE_IS_NOT_ENOUGH_AMOUNT,"There is not enough ammount");
            }
            transaction.setAmount(reqTransaction.getAmount()/100);
            response.setT(transaction);
            response.setStatus(RespStatus.succesMessage());

        }catch (UniTechException uniTechException) {
            uniTechException.printStackTrace();
            response.setStatus(new RespStatus(uniTechException.getCode(), uniTechException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }

    @Override
    public Response<List<RespTransaction>> getTransactionListByAccountId(ReqTransaction reqTransaction) {
        Response<List<RespTransaction>> response = new Response<>();
        try{
            Long accountId= reqTransaction.getDtAccount().getId();
            if (accountId == null) {
                throw new UniTechException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            Account account = accountRepository.findAccountByIdAndActive(accountId, Enum.Active.getValue());
            if (account == null) {
                throw new UniTechException(ExceptionConstants.ACCOUNT_NOT_FOUND, "Account not found");
            }
            List<Transaction> transactionList = transactionRepository.findAllByDtAccountAndActive(account, Enum.Active.getValue());
            if (transactionList.isEmpty()) {
                throw new UniTechException(ExceptionConstants.TRANSACTION_NOT_FOUND, "Transaction not found");
            }
            List<RespTransaction> respTransactionList = transactionList.stream().map(this::convert).collect(Collectors.toList());
            response.setT(respTransactionList);
            response.setStatus(RespStatus.succesMessage());
        }catch (UniTechException uniTechException) {
            uniTechException.printStackTrace();
            response.setStatus(new RespStatus(uniTechException.getCode(), uniTechException.getMessage()));
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return response;
    }
    private RespTransaction convert(Transaction transaction) {
        RespCustomer respCustomer = RespCustomer.builder()
                .id(transaction.getDtAccount().getCustomer().getId())
                .name(transaction.getDtAccount().getCustomer().getName())
                .surname(transaction.getDtAccount().getCustomer().getSurname())
                .build();
        RespAccount respAccount = RespAccount.builder()
                .id(transaction.getDtAccount().getId())
                .name(transaction.getDtAccount().getName())
                .accountNo(transaction.getDtAccount().getAccountNo())
                .iban(transaction.getDtAccount().getIban())
                .amount((double) transaction.getDtAccount().getAmount() / 100)
                .currency(transaction.getDtAccount().getCurrency())
                .respCustomer(respCustomer)
                .build();
        RespTransaction respTransaction = RespTransaction.builder()
                .id(transaction.getId())
                .receiptNo(transaction.getReceiptNo())
                .dtAccount(respAccount)
                .crAccount(transaction.getCrAccount())
                .amount(transaction.getAmount() / 100)
                .build();

        return respTransaction;
    }
}
